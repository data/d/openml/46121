# OpenML dataset: BitInfoCharts-clean-preprocessed

https://www.openml.org/d/46121

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Bitcoin data scrapped from BitInfoCharts, with preprocessing.

Several Bitcoin related data scrapped directly from BitInfoCharts. 'date' in the format %Y-%m-%d.
We have only keep the rows between the max(dates with non NaN values of each column) and min(dates with non NaN values of each column), which
leave us with dates between 2014-04-09 and 2023-03-14.

Preprocessing:

1 - Renamed columns to 'date' and 'value_X' with X from 0 to 18 (number of columns of original dataset).

2 - Created columns 'time_step' and 'id_series'. There is only one 'id_series' (0).

3 - Filled nan values by propagating the last valid observation to next valid (ffill).

The columns with some missing values were:
'confirmationtime': 'value_10'
'tweets': 'value_14'
'activeaddresses': 'value_16'
'top100cap': 'value_17'

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46121) of an [OpenML dataset](https://www.openml.org/d/46121). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46121/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46121/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46121/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

